let user = require( '../user' );

module.exports.registerRequest = async function ( req , res ) {
    if ( req.body.name && req.body.email && req.body.password ) {
        let user = await req.db.collection( 'user' ).findOne( { email: req.body.email } );
        if ( null !== user ) {
            return res.render( 'register' , { error: 'Пользователь с такой эл. почтой существует' } );
        }
        req.db.collection( 'user' ).insertOne( { name:req.body.name , email:req.body.email , password:req.body.password , ozonClientId:req.body.ozonClientId , ozonApiKey:req.body.ozonApiKey , wbUserName:req.body.wbUserName , wbPassword:req.body.wbPassword } , function ( error , user ) {
            if ( error ) {
                return res.sendStatus( 500 );
            } else {
                req.session.username = req.body.email;
                req.session.tsUser = req.body.email;
                req.session.userName = user.name;
                req.session.userId = user.ops[ 0 ]._id;
                req.session.validated = true;
                return res.redirect( '/' );
            }
        });
    } else {
        return res.render( 'register' , { error: 'Пожалуйста проверьте данные' } );
    }
}